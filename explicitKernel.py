import numpy as np
import pdb
import itertools

def kernel(X,degree,allUnique=False):
  result = None
  for d in range(1,degree+1):
    combs = itertools.combinations_with_replacement(range(X.shape[1]),d) #combinations with replacement
    result_d = None
    for c in combs:
      if allUnique:
        if len(set(c))<d: continue
      x = np.ones_like(X[:,0])
      for cc in c: x*=X[:,cc] 
      x = x.reshape(-1,1)
      if result_d is None: result_d = x
      else: result_d = np.hstack([result_d,x])
    if d==1: result = result_d
    else:
      if result_d is not None: result = np.hstack([result,result_d])
  return result

class explicitKernel():
  '''
  A tool for adding an explicit kernel in a model. 
  '''
  def __init__(self,model,degree=1,allUnique=False):
    self.model = model
    self.degree = degree
    self.allUnique = allUnique
  def fit(self,X,y):
    '''
    X: list of features as a horizontally stacked row of column vectors
    y: target values
    '''
    newX = kernel(X,self.degree,allUnique=self.allUnique)
    return self.model.fit(newX,y)
  def predict(self,X):
    '''
    X: list of features as a horizontally stacked row of column vectors
    '''
    try: newX = kernel(X,self.degree,allUnique=self.allUnique)
    except: newX = kernel(X,self.degree) #compatibility with old versions
    return self.model.predict(newX)

###
#Test:
#from sklearn import linear_model,neural_network
#def f(x,z):
#  return np.sqrt(x+x*z+z)
#  #return np.sqrt(x)+np.sqrt(x*z)+np.sqrt(z)
#np.random.seed(1)
#N = 10000
#x = np.random.uniform(0,10,N)
#z = np.random.uniform(0,10,N)
#sigma = 0.1
#y = np.random.normal(f(x,z),sigma)
#
#X = x.reshape(-1,1)
#Z = z.reshape(-1,1)
#print np.hstack([X,Z])
#
###lr = linear_model.LinearRegression()
#lr = neural_network.MLPRegressor(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(10,), random_state=0,warm_start=False,activation='relu',max_iter=2000)
###test:
##e = explicitKernel(lr,degree=2,allUnique=True) #using degree 1 is way worse because it can't do interaction terms easily
#e = explicitKernel(lr,2,allUnique=True) #using degree 1 is way worse because it can't do interaction terms easily
#e.fit(np.hstack([X,Z]),y)
###print e.model.coef_
#print e.model.loss_
#closure = e.predict(np.hstack([X,Z]))/y
#print np.mean(closure),np.std(closure)
#####
