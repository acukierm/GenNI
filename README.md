# Readme
The idea of generalized numerical inversion is implemented in a package ```genNumericalInversion```, which can be found in this repository. In the package there is just a single class member ```genNumericalInverter```, which is the tool that implements the idea.

There is an [example script](testGenNI.py) which shows a (complete) example of using the tool:

```
python testGenNI.py

Learning loss:
24.3442311856

Inverting...
Done fitting inversion! Loss:  0.0125795409918
Technical closure: 1.00001312533 0.00552778354584
Testing technical closure:  1.00001312533 0.00552778354584
Closure:  0.99668079801 0.264173189051
Can save gNI object and load it
...
```
In the test script, the following steps are performed:
1. Declaring a list of truth pT values *X*, reco pT values *Y*, and (possible many) features of interest *Z* (for real studies, these values must come from some analysis code and then converted to numpy arrays - that's up to you!).
2. Training a network ```lr``` to learn *Y* given *X* and *Z*.
3. Importing the ```genNumericalInverter``` class, and calling ```help``` on the class to print out the documentation (please read!).
4. Initializing a ```genNumericalInverter``` object ```genNI``` with the *trained* network ```lr``` and an *untrained* network ```lr2``` which indicates the architecture you want for the inversion network (not necessarily the same as the architecture for ```lr```).
5. Calling the ```genNumericalInverter``` function ```fit``` in order to perform the inversion, passing in *X* and *Z* (not *Y*!).
6. Using the ```genNumericalInverter``` function ```predict``` on *Y* and *Z* in order to test the "technical closure" of the inversion and get a list of calibrated pTs and test the normal "closure".
7. Dumping and loading the ```genNumericalInverter``` object into a pickle file.
8. Some other printout statements to check the behavior of the calibration function outside of the region it was trained on (technical details).

## Technical Details
What is the ```genNumericalInverter``` object doing? Basically all it does it train the second network ```lr2``` to learn *X* given the learned values *L(X,Z)* from the first network ```lr``` (here referred to as *L*).

However there is one technical detail, which is the behavior of the calibration outside of the region it was trained on (i.e., for true pT values outside of the range (*min(X),max(x)*) and reco pT values outside of the range (*min(L(x)),max(L(x))*). Since the network was not trained on this region, the behavior of the network is undefined. However the behavior of the calibration is crucially important in this region, since we still need to calibrate reco pT values that fall outside this range. To solve this problem we use a linear extrapolation of the calibration function outside of the trained region, which [has been shown](https://arxiv.org/abs/1609.05195) to generally lead to good behavior of the calibration.

## Explicit Kernel
In practical testing it has been found that often the response can be expressed as depending on *XZ*, or as products of the features *Z1Z2*. A neural network can of course learn arbitrary functions of its inputs, if it's deep and wide enough. However, if the behavior of the learned function is known in advance, an explicit kernel multiplying these features together can be used in order to cut down on the size of the network and reduce training time.

An additional package, ```explicitKernel```, is also provided in order to implement this shortcut, and a test script ```testGenNI_wEK.py``` showing the use of this explicit kernel in the context of the ```genNumericalInverter``` tool is also provided. The ```explicitKernel``` class is basically just a wrapper around your normal learning model which adds this kernel of your input features explicitly.